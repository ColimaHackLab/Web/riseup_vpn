<style>
h1 {font-size: 2em;text-align: center}
h2 {font-size: 1.5em;}
figcaption {text-align: center}
</style>

# RiseupVPN en OpenWrt con OpenVPN

Este tutorial es para configurar RiseupVPN en un _router_ con
OpenWrt y OpenVPN.

Esto permite navegar a través de una +++VPN+++ en cualquier dispositivo
que esté conectado al _router_ sin necesidad de instalar _software_
adicional.

Es decir, brinda mayor privacidad al acceder a internet porque
se usa un «túnel» donde tu información es inaccesible para tu
proveedor de internet.

## Archivos

* [GitLab](https://gitlab.com/NikaZhenya/riseup_vpn)
* [GitHub](https://github.com/NikaZhenya/riseup_vpn)
* [EPUB](https://gitlab.com/NikaZhenya/riseup_vpn/raw/master/out/epub.epub)
* [MOBI](https://gitlab.com/NikaZhenya/riseup_vpn/raw/master/out/mobi.mobi)

## Índice

[Elementos necesarios](#elementos-necesarios) \
[1. Obtención de credenciales de Riseup](#obtencion-de-credenciales-de-riseup) \
[2. Instalación de OpenVPN](#instalacion-de-openvpn) \
[3. Configuración de OpenVPN](#configuracion-de-openvpn) \
[4. Configuración de la interfaz](#configuracion-de-la-interfaz) \
[5. Configuración del _firewall_](#configuracion-del-firewall) \
[6. Comprobación de arranque de RiseupVPN](#comprobacion-de-arranque-de-riseupvpn) \
[Últimos retoques](#ultimos-retoques) \
[Automatización](#automatizacion) \
[Addenda 1. Fugas en el DNS, de IPv6 o de WebRTC](#addenda-1-fugas-en-el-dns-de-ipv6-o-de-webrtc) \
[Addenda 2. Cambio de DNS](#addenda-2-cambio-de-dns) \
[Addenda 3. Archivo de configuración CONF u OVPN](#addenda-3-archivo-de-configuracion-conf-u-ovpn) \
[Recursos](#recursos)

## Elementos necesarios

* _Router_ con [OpenWrt](https://openwrt.org/).
* Cuenta de [Riseup](https://account.riseup.net/).

## 1. Obtención de credenciales de Riseup

En esta primera sección vamos a obtener de Riseup:

1. Usuario y contraseña para la +++VPN+++.
2. Certificado +++CRT+++.

### 1.1. Obtención de usuario y contraseña

__a.__ Ingresa a [tu cuenta](https://account.riseup.net/) de
Riseup.

![Portal de cuentas de Riseup.](../img/img01.png)

__b.__ Ve al área `Contraseñas > Contraseñas de servicio`.

![Panel de contraseñas de servicio.](../img/img02.png)

__c.__ Introduce una nueva contraseña o genérala de manera aleatoria
y haz clic en `Guardar`.

__d.__ Guarda en un archivo tu usuario y contraseña desde la
[terminal](https://es.wikipedia.org/wiki/Terminal_(inform%C3%A1tica)):
{.espacio-arriba1 .sin-sangria}

```
vim auth.txt
```

En la primera línea del archivo guardas tu usuario _sin_ `@riseup.net`
y, en la segunda, tu contraseña.

![Ejemplo del archivo `auth.txt`.](../img/img03.png)

> +++OJO+++: para guardar y salir de `vim` teclea dos puntos (`:`), luego (`x`) y por último `Enter`.

### 1.2. Obtención del certificado +++CRT+++

__e.__ Descarga el certificado +++CRT+++:

```
wget https://riseup.net/security/network-security/riseup-ca/RiseupCA.pem -O ca.crt
```

__f.__ Manda las credenciales al _router_:

```
scp auth.txt root@192.168.1.1: && scp ca.crt root@192.168.1.1:
```

> +++OJO+++: cambia la +++IP+++ `192.168.1.1` por la de tu _router_.

Ahora estamos listos para instalar y configurar la +++VPN+++.

## 2. Instalación de OpenVPN

En esta sección instalaremos OpenVPN en el _router_ para usar
RiseupVPN.

__g.__ Ingresa al _router_ a través de +++SSH+++:
{.espacio-arriba1 .sin-sangria}

```
ssh root@192.168.1.1
```

> +++OJO+++: cambia la +++IP+++ `192.168.1.1` por la de tu _router_.

__h.__ Actualiza la lista de paquetes de OpenWrt:

```
opkg update
```

__i.__ Instala OpenVPN y su interfaz _web_:

```
opkg install openvpn-openssl luci-app-openvpn
```

__j.__ Coloca los archivos `auth.txt` y `ca.crt` en el directorio
adecuado:

```
mv *.* /etc/openvpn/
```

Ahora estamos listos para configurar la +++VPN+++.

## 3. Configuración de OpenVPN

En esta sección configuraremos OpenVPN para usar RiseupVPN.

__k.__ Elimina el archivo de configuración de OpenVPN y abre
uno nuevo: {.espacio-arriba1 .sin-sangria}

```
rm /etc/config/openvpn && vim /etc/config/openvpn
```

__l.__ Pega la siguiente configuración:

```
config openvpn 'riseup_client'
        list remote '198.252.153.226'
        list remote 'vpn.riseup.net'
        option auth 'SHA256'
        option auth_nocache '1'
        option auth_user_pass '/etc/openvpn/auth.txt'
        option ca '/etc/openvpn/ca.crt'
        option cipher 'AES-256-CBC'
        option client '1'
        option dev 'tun'
        option enabled '1'
        option nobind '1'
        option persist_key '1'
        option persist_tun '1'
        option port '80'
        option proto 'udp'
        option remote_cert_tls 'server'
        option resolv_retry 'infinite'
        option script_security '2'
```

Ahora estamos listos para configurar la interfaz.

## 4. Configuración de la interfaz

En esta sección vamos a configurar la interfaz para que RiseupVPN
funcione correctamente.

> +++OJO+++: con probabilidad en este punto o más adelante perderás
conexión a internet.

__m.__ Abre la configuración de las interfaces:

```
vim /etc/config/network
```

__n.__ Pega la siguiente configuración:

```
config interface 'vpn'                
        option proto 'none'  
        option ifname 'tun0'         
        option auto '1'
        option delegate '0'
```

Ahora estamos listos para configurar el _firewall_.
{.espacio-arriba1 .sin-sangria}

## 5. Configuración del _firewall_

En esta sección vamos a configurar el _firewall_ para que
RiseupVPN funcione correctamente.

__o.__ Abre la configuración del _firewall_:
{.espacio-arriba1 .sin-sangria}

```
vim /etc/config/firewall
```

__p.__ Pega la siguiente configuración al final del archivo:

```
config zone                             
        option name 'vpn'                   
        option forward 'REJECT'                 
        option output 'ACCEPT'         
        option network 'vpn'          
        option input 'REJECT'                   
        option masq '1'                     
        option mtu_fix '1'                  
                                               
config forwarding                            
        option dest 'vpn'                       
        option src 'lan'
```

__q.__ Reinicia el _router_:

```
reboot
```

## 6. Comprobación de arranque de RiseupVPN

En esta última sección ya deberías de tener la +++VPN+++ activa.
Empecemos a comprobar.

__r.__ Escribe la +++IP+++ del _router_ en tu explorador
para ingresar a la interfaz _web_ de OpenWrt.
{.espacio-arriba1 .sin-sangria}

> +++OJO+++: revisa que estés conectado al internet desde el _router_.

![Portal de ingreso de OpenWrt.](../img/img04.png)

__s.__ Ve a `Services > OpenVPN`. {.espacio-arriba1 .sin-sangria}

__t.__ Verifica que en la columna `started` de `riseup_client` diga
`yes`. {.espacio-arriba1 .sin-sangria}

![RiseupVPN activado en OpenWrt con OpenVPN.](../img/img14.png)

¡Felicidades, ya estás conectado a internet por medio de RiseupVPN
configurado en tu _router_ con OpenWrt y OpenVPN!
{.espacio-arriba1 .centrado}

## Últimos retoques

__u.__ Comprueba que estás en internet por medio de RiseupVPN:

```
traceroute perrotuerto.blog
```

> +++OJO+++: puedes escribir cualquier dominio en lugar de [perrotuerto.blog](http://perrotuerto.blog).

En los primeros pasos la ruta debería transitar por [Riseup.net](https://riseup.net/).
Si no es el caso, _la +++VPN+++ no está en funcionamiento_.

![Ejemplo de `traceroute` donde el tercer paso explicita el tránsito por RiseupVPN.](../img/img15.png)

Otra manera de comprobar es ver la +++IP+++ de tu dispositivo
con [ipapi](https://ipapi.co/).

![Portal de ipapi donde se explicita el uso de RiseupVPN.](../img/img16.png)

__v.__ Reinicia el _router_ si tienes alguno de los siguientes
problemas: {.espacio-arriba1 .sin-sangria}

* No tienes internet.
* La +++VPN+++ no inicia.
* La +++VPN+++ no funciona.

## Automatización

Los pasos seguidos pueden automatizarse. Para ello está el _script_
[`install`](https://gitlab.com/NikaZhenya/riseup_vpn/raw/master/sh/install.sh),
disponible en el directorio [`sh`](https://gitlab.com/NikaZhenya/riseup_vpn/tree/master/sh)
del [repositorio](https://gitlab.com/NikaZhenya/riseup_vpn).

Una vez descargado o copiado, desde el _router_ ejecuta:

```
sh install.sh [-u usuario] [-p contraseña]
```

OJO: el usuario y contraseña corresponde a tu cuenta de Riseup. Ve los
puntos [__a—c__ del tutorial](#obtencion-de-usuario-y-contrasena).

## Addenda 1. Fugas en el DNS, de IPv6 o de WebRTC

Cualquier conexión a una +++VPN+++ puede ocasionar fugas que pueden
poner en riesgo tu privacidad. Para comprobar si tienes alguna, ve a:

* [DNS leak](https://www.dnsleaktest.com)
* [IP leak](https://ipleak.net/)

### Solución de fugas en el DNS

La configuración recomendada por Riseup incluye un _script_ que
evita las fugas +++DNS+++. Sin embargo, este requiere `resolvconf` y
al parecer OpenWrt no lo admite. Para solucionar este problema puede
ejecutarse lo siguiente:

```
uci set dhcp.@dnsmasq[0].noresolv="1" && uci commit dhcp && killall dnsmasq && /etc/init.d/dnsmasq start
```

Ya debería quedar solucionado.
De nuevo comprueba varias veces con [DNS leak](https://www.dnsleaktest.com) o
[IP leak](https://ipleak.net/) si cuentas con fugas.

### Solución de fugas de IPv6

Una solución para evitar fugas de +++IP+++v6 es su completa deshabilitación.
Para realizarlo, ejecuta:

```
uci set 'network.lan.ipv6=off' && uci set 'network.wan.ipv6=off' && uci set 'dhcp.lan.dhcpv6=disabled' && /etc/init.d/odhcpd disable && uci commit && killall dnsmasq && /etc/init.d/dnsmasq start
```

Ya debería quedar solucionado.
De nuevo comprueba varias veces con [IP leak](https://ipleak.net/)
si cuentas con fugas.

### Solución de fugas de WebRTC

Para solucionarlo es necesario deshabilitar Web+++RTC+++.

* Si usas Mozilla Firefox, ve a `about:config` y deshabilita
  `media.peerconnection.enabled`.

* Si usas Google Chrome, instala la extensión
  [Web+++RTC+++ Network Limiter](https://chrome.google.com/webstore/detail/webrtc-network-limiter/npeicpdbkakmehahjeeohfdhnlpdklia).

## Addenda 2. Cambio de DNS

Si buscas una mejor alternativa de +++DNS+++ al ofrecido por
Google u otras entidades, puedes probar con
[Open+++NIC+++](https://www.opennic.org/). Para cambiarlo, ejecuta:

```
uci add_list dhcp.@dnsmasq[-1].server='66.187.76.168' && uci add_list dhcp.@dnsmasq[-1].server='206.189.168.3' && uci add_list dhcp.@dnsmasq[-1].server='172.98.193.42' && uci commit && killall dnsmasq && /etc/init.d/dnsmasq start
```

## Addenda 3. Archivo de configuración CONF u OVPN

Para ciertos clientes es necesario ingresar un archivo de
configuración. Para sistemas UNIX la extensión suele ser `.conf`.
Para Windows suele tener la extensión `.ovpn`. 

En el directorio [`conf`](https://gitlab.com/NikaZhenya/riseup_vpn/tree/master/conf)
del [repositorio](https://gitlab.com/NikaZhenya/riseup_vpn)
se encuentra el archivo de configuración, el cual tienes que:

1. Cambiar el nombre de extensión según el sistema operativo.
2. Guardar un archivo `auth.txt` donde la primera línea es tu
   usuario sin `@riseup.net` y la segunda, tu contraseña.
3. Modificar la ruta a `auth.txt` en la línea 5 de este archivo
   de configuración.
4. ¡Listo!

## Recursos

* «[Disable +++IP+++v6 on OpenWrt](https://spotlightcybersecurity.com/disable-ipv6-on-openwrt.html)».
* «[+++DNS+++ leak test](https://www.dnsleaktest.com/)».
* «[+++DNS+++ over +++HTTPS+++ Prox](https://www.reddit.com/r/openwrt/comments/erzpnd/dns_over_https_proxy_settings_dns_still_leaks_to/ff6x3b6/)».
* «[Dnsmasq](https://openwrt.org/es/doc/howto/dhcp.dnsmasq)».
* «[+++IP/DNS+++ Detect](https://ipleak.net/)».
* «[OpenNIC Project](https://www.opennic.org/)».
* «[OpenVPN client with LuCi web +++GUI+++](https://openwrt.org/docs/guide-user/services/vpn/openvpn/client-luci)».
* «[RiseupVPN +++GNU+++/Linux](https://riseup.net/en/vpn/vpn-red/linux)».
* «[How to Setup OpenVPN on OpenWrt](https://hide.me/en/vpnsetup/openwrt/openvpn/)».
