#!/usr/bin/env bash
# Instalación de
# RiseupVPN en OpenWrt con OpenVPN
# Usa el parámetro -h para más información

PROGRAM="$(basename -- "$0")"
VPN_USER=""
VPN_PASSWORD=""

# Verifica los parámetros
function check () {
  if [ ! -n "${VPN_USER}" ] || [ ! -n "${VPN_PASSWORD}" ]; then
    echo "ERROR: especifica usuario y contraseña de RiseupVPN."
    echo "Usa 'sh ${PROGRAM} -h' para más información."
    exit 1
  fi
}

# Realiza la instalación
function install () {
  echo "Actualizando lista de paquetes…"
  opkg update
  echo "Instalando OpenVPN y su aplicación web de OpenWrt"
  opkg install openvpn-openssl luci-app-openvpn
}

# Añade las credenciales
function credentials () {
  echo "Añadiendo credenciales de RiseupVPN…"

  # Archivo con usuario y contraseña
  cat > /etc/openvpn/auth.txt << EOF
$VPN_USER
$VPN_PASSWORD
EOF

  # Certificado de RiseupVPN
  cat > /etc/openvpn/ca.crt << EOF
-----BEGIN CERTIFICATE-----
MIIF2jCCA8KgAwIBAgIIVogyQTSIzc8wDQYJKoZIhvcNAQELBQAwgYYxGDAWBgNV
BAMTD1Jpc2V1cCBOZXR3b3JrczEYMBYGA1UEChMPUmlzZXVwIE5ldHdvcmtzMRAw
DgYDVQQHEwdTZWF0dGxlMQswCQYDVQQIEwJXQTELMAkGA1UEBhMCVVMxJDAiBgkq
hkiG9w0BCQEWFWNvbGxlY3RpdmVAcmlzZXVwLm5ldDAiGA8yMDE2MDEwMjIwMjU0
MFoYDzIwMjYwMzMwMjAyNjAxWjCBhjEYMBYGA1UEAxMPUmlzZXVwIE5ldHdvcmtz
MRgwFgYDVQQKEw9SaXNldXAgTmV0d29ya3MxEDAOBgNVBAcTB1NlYXR0bGUxCzAJ
BgNVBAgTAldBMQswCQYDVQQGEwJVUzEkMCIGCSqGSIb3DQEJARYVY29sbGVjdGl2
ZUByaXNldXAubmV0MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAw2VV
uoz4xqeB1ROIwXBRaj0prOqEFX89A7+2rslGRfjM8NPHyBLGleoHTK3DPwadtQeg
ulaEOAjM5EMXTEX/o9H46L6h729HUWPCwVssvvOjyxTyGJDf7Ihd/Ab7ODtlJSyc
g31aXMioA5pGz5QnS3VGz4nE9+NL+jobc/NbhaacsEPR/7xO7meRNu/1S+YiHK1y
BSVrfap3XItlcNHDGNQkPyyJbS3pAS1lQs2HCBTzcFCamCkDOC7cRh9wZ4GH8U2f
2s0mDD5zhRpheNW4gFBtGpqHiRXv7WJW612aaXzKQQoIq2loGNvOpnyBPKL3jjUT
Rxv5IzWMV0nAofMCy25u/S4J65uSEd9mLNXFJ3rl+cFaybcOUXktTbS7bZy6cMyf
/gO28bEXIWr5WfZf8jCbPyOVfExZquG3aS+0YPWmIJCheXQzgiwplZy93oND1GGQ
f+1R2F7GPwNXQdefv2xm7PTWhHbSWHHmeY89qYED+yFJrX5ChoFoBbYs1lMmdU/C
2MnQBFtvcVockXFAUONyMKiq8ZP6sQ1lu0rO9Bvkhx55sJLZOmjN3g4S1K97PbbI
5DzHKcR0JQSt8ZtCY/MuMbwvlNYo98bFWvlfKET0KPtogNNH0PNfJmStKR8jWGjE
HnUNXo7YDfK90iEKTjLz2K5CYzH5Dm6iYJNaaykCAwEAAaNGMEQwEgYDVR0TAQH/
BAgwBgEB/wIBADAPBgNVHQ8BAf8EBQMDBwQAMB0GA1UdDgQWBBTGek7ebtq2Ibm+
2K6je1IMobvEkzANBgkqhkiG9w0BAQsFAAOCAgEAO2B3jnL+8LeoRkc282qUpHyu
xYj0Qd68l0CJ0FjfA2OCR/6h1W4gZVH+fTd/mhgrNXj28GRT53JEh1jdRC7ENTXu
W9O8I9gCbWQ6V4nkZ9lpq8UEmKTFGnngVu8VCmSDF+y0kFuEtmt0jyd2UkJfC/vy
Gh78OCHEdGAeOTYHXamiuA9Z7wMuncPjP476gSW2kfWTdxV25ad4tT5dA5d42xDm
YE2UKzHeB9amOmvyh08LPD0idT5oROCIHsHBhQC9oltJXO5j6GyHRg88C1inyv6R
xk+w9ek4wSBpoJg5t3hdbZr3JTUsuu4WPtAET0fMQpJC+niaBbegwtvdLZFM+d8x
ead3ZpMO+XrpazDFGtdPTQdi5EIYmr2RL9eTeQbVPwMB9TgFpBXP+iYIuTpNo8jn
8zS4EcPRmz6PQJVK4zkHczfvquyU9RuOwEgb8qN4tSNxF0Z94uSVUoXCG9WZLf8q
MfsGesYiR/qLnLn3MfAyWm3OVOUvGzczDE2T8VvY7rXc2+8ra5aK0TNAgEz9ey6D
/dGzM1JCCe1A08s+2+eRX//pmqmOCoGrY7zwIVS2T249h6iIMM9yT0C3ZXRoTnVN
osyidOkVuQr0YK6shJ0WaK4F1MktdjOZKPoIc9QLw+TrSU2hfyla36T0bNWMC/TJ
YtxDI+d1jIFZ7zMmts4=
-----END CERTIFICATE-----
EOF
}

# Configura todo lo necesario
function configure () {
  echo "Configurando datos…"

  # Configuración de la VPN
  cat > /etc/config/openvpn << EOF

config openvpn 'riseup_client'
        list remote '198.252.153.226'
        list remote 'vpn.riseup.net'
        option auth 'SHA256'
        option auth_nocache '1'
        option auth_user_pass '/etc/openvpn/auth.txt'
        option ca '/etc/openvpn/ca.crt'
        option cipher 'AES-256-CBC'
        option client '1'
        option dev 'tun'
        option enabled '1'
        option nobind '1'
        option persist_key '1'
        option persist_tun '1'
        option port '80'
        option proto 'udp'
        option remote_cert_tls 'server'
        option resolv_retry 'infinite'
        option script_security '2'

EOF

  # Configuración de la interfaz
  cat >> /etc/config/network << EOF

config interface 'vpn'
        option proto 'none'
        option ifname 'tun0'
        option auto '1'
        option delegate '0'

EOF

  # Configuración del firewall
  cat >> /etc/config/firewall << EOF

config zone
        option name 'vpn'
        option forward 'REJECT'
        option output 'ACCEPT'
        option network 'vpn'
        option input 'REJECT'
        option masq '1'
        option mtu_fix '1'

config forwarding
        option dest 'vpn'
        option src 'lan'

EOF
}

# Concluye la actualización
function end () {
  echo "Instalación finalizada."
  echo "Reinicia el router para aplicar los cambios."
}

# Muestra la ayuda
function help () {
  echo "${PROGRAM} -- Instalación de RiseupVPNen OpenWrt con OpenVPN."
  echo
  echo 'Uso:'
  echo "  sh ${PROGRAM} [-u usuario] [-p contraseña]"
}

# Obtiene los valores
while getopts hu:p: option; do
  case ${option} in
    h) help ; exit ;;
    u) VPN_USER="${OPTARG}" ;;
    p) VPN_PASSWORD="${OPTARG}" ;;
  esac
done

check
install
credentials
configure
end
